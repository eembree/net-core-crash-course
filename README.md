# A Crash Course in .NET (Core)

This repository is part of a quick crash course is ASP.NET Core setup and features for developers already familiar with the .NET Framework version of ASP.NET

See the tags on the main branch which line up with the slides which say Demo and list the tag to check out.

This code was tested with .NET SDK 6.0.2. You can run the code with either Visual Studio 2022+, JetBrains Rider, or from the command line by running `dotnet run` from src/NetCoreCrashCourse.

To see the example of configuration reading an environment variable you will have to run the app with the EnvVarConfig launch profile. This is selected from a dropdown in VS and Rider and on the commandline can be run with `dotnet run --launch-profile EnvVarConfig`.

To See the example of configuration being read from appsettings.Development.Local.json overriding earlier configuration you will have to create the file appsettings.Development.Local.json (it is deliberately ignored in .gitignore) in src/NetCoreCrashCourse and add json content like the following.
```json
{
  "Greeting": {
    "Message": "Hello Development Local!",
    "Secret": "MySecret123"
  }
}
```
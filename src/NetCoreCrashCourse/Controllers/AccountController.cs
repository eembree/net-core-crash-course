﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using NetCoreCrashCourse.Library;
using NetCoreCrashCourse.Models;

namespace NetCoreCrashCourse.Controllers
{
    public class AccountController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AccountController(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet]
        public IActionResult Login(string? returnUrl)
        {
            return View(new LoginModel()
            {
                ReturnUrl = returnUrl
            });
        }

        public async Task<IActionResult> Login(LoginModel model)
        {
            if (!ModelState.IsValid) return View();

            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, model.UserName!)
            };

            if ("MyClaims".Equals(model.UserName, StringComparison.CurrentCultureIgnoreCase))
            {
                claims.AddRange(new Claim[]
                {
                    new Claim(ClaimTypes.GivenName, "My"),
                    new Claim(ClaimTypes.Surname, "Claims"),
                    new Claim(ClaimTypes.Role, "Admin"),
                    new Claim(MyClaims.MyCustomClaim, "1,2"),
                    new Claim(MyClaims.MyCustomClaim, "4"),
                    new Claim(MyClaims.MyCustomClaim, "42"),
                    new Claim(MyClaims.HasAccessToItem, "1")
                });
            }

            var claimsIdentity = new ClaimsIdentity(
                claims: claims,
                authenticationType: CookieAuthenticationDefaults.AuthenticationScheme);

            var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

            _httpContextAccessor.HttpContext!.User = claimsPrincipal;

            await _httpContextAccessor.HttpContext.SignInAsync(
                scheme: CookieAuthenticationDefaults.AuthenticationScheme,
                principal: claimsPrincipal);


            if (string.IsNullOrWhiteSpace(model.ReturnUrl) || !Url.IsLocalUrl(model.ReturnUrl))
            {
                return RedirectToAction("Index", "Home");
            }

            return Redirect(model.ReturnUrl);
        }

        public async Task<IActionResult> Logout()
        {
            if (User.Identity is { IsAuthenticated: true })
            {
                await _httpContextAccessor.HttpContext!.SignOutAsync();
            }

            return RedirectToAction("Index", "Home");
        }
    }
}

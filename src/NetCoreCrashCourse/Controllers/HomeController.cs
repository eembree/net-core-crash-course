﻿using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NetCoreCrashCourse.Library;
using NetCoreCrashCourse.Models;

namespace NetCoreCrashCourse.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    private readonly IReadOnlyList<Item> _dataStore;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
        _dataStore = new List<Item>()
        {
            new Item() { Id = 1, Name = "First Item" },
            new Item() { Id = 2, Name = "Second Item" }
        };
    }

    public IActionResult Index() => View();

    public IActionResult Privacy() => View();

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error() => View(new ErrorViewModel
    {
        RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier
    });

    public IActionResult Config()=> View();

    [Authorize]
    public IActionResult RequireLogin() => View();

    [Authorize(MyPolicies.HasAdminRole)]
    public IActionResult Admin() => View();

    public IActionResult AccessDenied() => View();

    [Authorize(MyPolicies.HasClaimOne)]
    public IActionResult RequireCustomClaimOne() => View();

    [Authorize(MyPolicies.HasClaimTwoAndThree)]
    public IActionResult RequireCustomClaimTwoAndThree() => View();

    [Authorize(MyPolicies.HasClaimFour)]
    public IActionResult RequireCustomClaimFour() => View();

    public async Task<IActionResult> ClaimFortyTwo([FromServices] IAuthorizationService authService)
    {
        var authResult = await authService.AuthorizeAsync(User, MyPolicies.HasClaimFortyTwo);
        string content = authResult.Succeeded
            ? "Answer to the Ultimate Question of Life, the Universe, and Everything"
            : "So long and thanks for all the fish";
        return View(model: content);
    }

    
    public async Task<IActionResult> HasAccessToItem(
        [FromRoute] int id,
        [FromServices] IAuthorizationService authService)
    {
        var item = _dataStore.FirstOrDefault(x => x.Id == id);
        if (item == null) return NotFound();

        var authResult = await authService.AuthorizeAsync(User, item, MyPolicies.CanAccessItem);

        if (!authResult.Succeeded)
        {
            _logger.LogCritical("Uh oh, security violation!");
            return Forbid();
        }
        
        return View(item);
    }
}
// 1. Build a 'web app builder'

using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using NetCoreCrashCourse.Library;

var builder = WebApplication.CreateBuilder(args);

// 2. *Adjust configuration
builder.Configuration.AddJsonFile(
    path: "appsettings.Development.Local.json",
    optional: true,
    reloadOnChange: true);

// 3. *Add services
builder.Services.AddControllersWithViews();

builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
    .AddCookie(options =>
    {
        options.AccessDeniedPath = "/Home/AccessDenied";
        options.LoginPath = "/Account/Login";
        options.LogoutPath = "/Account/Logout";
        options.Cookie.Name = ".NetCoreCrashCourse.Auth";
    });

builder.Services.AddScoped<IAuthorizationHandler, HasClaimTypeWithValueHandler>();

builder.Services.AddScoped<IAuthorizationHandler, CanAccessItemHandler>();

builder.Services.AddAuthorization(options =>
{
    options.AddPolicy(MyPolicies.HasAdminRole,
        policyBuilder => policyBuilder.RequireClaim(ClaimTypes.Role, "Admin"));

    options.AddPolicy(MyPolicies.HasClaimOne, policyBuilder =>
    {
        policyBuilder.AddRequirements(new HasClaimTypeWithValueRequirement(MyClaims.MyCustomClaim, 1));
    });

    options.AddPolicy(MyPolicies.HasClaimTwoAndThree, policyBuilder =>
    {
        policyBuilder.AddRequirements(
            new HasClaimTypeWithValueRequirement(MyClaims.MyCustomClaim, 2),
            new HasClaimTypeWithValueRequirement(MyClaims.MyCustomClaim, 3));
    });

    options.AddPolicy(MyPolicies.HasClaimFour, policyBuilder =>
    {
        policyBuilder.AddRequirements(new HasClaimTypeWithValueRequirement(MyClaims.MyCustomClaim, 4));
    });

    options.AddPolicy(MyPolicies.HasClaimFortyTwo, policyBuilder =>
    {
        policyBuilder.AddRequirements(new HasClaimTypeWithValueRequirement(MyClaims.MyCustomClaim, 42));
    });

    options.AddPolicy(MyPolicies.CanAccessItem, policyBuilder =>
    {
        policyBuilder.AddRequirements(new CanAccessItemRequirement());
    });
});

builder.Services.AddHttpContextAccessor();

// 4. 'Build' the web app
var app = builder.Build();

// 5. Configure the middleware pipeline
//----------- BEGIN MIDDLEWARE -----------

// Show a stock error page when not in a development environment. Otherwise the default error page shows details of any
// thrown exception
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");

    // Request from browser to send all requests via HTTPS
    app.UseHsts();
}

// Change http://* to https://* and stop the pipeline
app.UseHttpsRedirection();
// Does the request match a file under wwwroot? If yes respond with the file and stop the pipeline.
app.UseStaticFiles();

// Enables use of standard MVC routing. This is split up from the actual endpoints
// the routing might match to (see ENDPOINTS below).
app.UseRouting();

// NOTE: At this point any middleware has access to any endpoint matched by UseRouting!

// Must come before use authorization or any authentication data will have been ignored
app.UseAuthentication();

// Enable authorization. In particular
// - mapping cookies/headers/etc. to the HttpRequest's User property
// - Redirect to login for standard cookie based login
// NOTE: The [Authorize] attribute on an endpoint won't be respected if UseRouting didn't find it before this point.
//  In other words, UseAuthorization must be after UseRouting. ORDER MATTERS!
app.UseAuthorization();

// ENDPOINTS
// Map different types of endpoints here. Most common will be MapControllerRoute() and MapRazorPages()
// Note: MapControllerRoute won't work without AddControllersWithViews or AddControllers above when registering services!
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
//----------- END MIDDLEWARE -----------

// 6. Run
app.Run();
﻿namespace NetCoreCrashCourse.Library
{
    public static class MyPolicies
    {
        public const string HasAdminRole = "HasAdminRole";
        public const string HasClaimOne = "HasClaimOne";
        public const string HasClaimTwoAndThree = "HasClaimTwoAndThree";
        public const string HasClaimFour = "HasClaimFour";
        public const string HasClaimFortyTwo = "HasClaimFortyTwo";
        public const string CanAccessItem = "CanAccessItem";
    }
}

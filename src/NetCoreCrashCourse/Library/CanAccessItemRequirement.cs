﻿using Microsoft.AspNetCore.Authorization;

namespace NetCoreCrashCourse.Library
{
    public class CanAccessItemRequirement : IAuthorizationRequirement
    {
    }

    public class CanAccessItemHandler : AuthorizationHandler<CanAccessItemRequirement, Item>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, CanAccessItemRequirement requirement, Item resource)
        {
            if (context.User.Identity is { IsAuthenticated: true })
            {
                string itemId = resource.Id.ToString();
                if (context.User.Claims.Any(claim => claim.Type == MyClaims.HasAccessToItem && claim.Value == itemId))
                {
                    context.Succeed(requirement);
                }
            }

            return Task.CompletedTask;
        }
    }
}

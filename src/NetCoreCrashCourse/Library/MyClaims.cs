﻿namespace NetCoreCrashCourse.Library
{
    public static class MyClaims
    {
        public const string MyCustomClaim = "MyCustomClaim";
        public const string HasAccessToItem = "HasAccessToItem";
    }
}

﻿using Microsoft.AspNetCore.Authorization;

namespace NetCoreCrashCourse.Library
{
    public class HasClaimTypeWithValueRequirement : IAuthorizationRequirement
    {
        public HasClaimTypeWithValueRequirement(string claimType, int value)
        {
            ClaimType = claimType;
            Value = value;
        }

        public string ClaimType { get; }
        public int Value { get; }
    }

    public class HasClaimTypeWithValueHandler : AuthorizationHandler<HasClaimTypeWithValueRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, HasClaimTypeWithValueRequirement requirement)
        {
            if (!string.IsNullOrWhiteSpace(requirement.ClaimType))
            {
                var matchedClaims = context.User.Claims
                    .Where(claim => requirement.ClaimType.Equals(claim.Type, StringComparison.CurrentCulture));
                
                foreach (var claim in matchedClaims)
                {
                    if (ContainsValue(claim.Value, requirement.Value))
                    {
                        context.Succeed(requirement);
                        break;
                    }
                }
            }

            return Task.CompletedTask;
        }

        private bool ContainsValue(string claimValue, int requirementValue)
        {
            try
            {
                return claimValue
                    .Split(",")
                    .Select(int.Parse)
                    .Any(v => v == requirementValue);
            }
            catch (Exception)
            {
                // ignored
            }

            return false;
        }
    }
}

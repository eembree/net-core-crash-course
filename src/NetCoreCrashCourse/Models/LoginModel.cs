﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace NetCoreCrashCourse.Models
{
    public class LoginModel
    {
        [Required]
        public string? UserName { get; set; }

        [HiddenInput]
        public string? ReturnUrl { get; set; }
    }
}
